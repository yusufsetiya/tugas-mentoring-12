<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Mentoring 12</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-T3c6CoIi6uLrA9TneNEoa7RxnatzjcDSCmG1MXxSR1GAsXEV/Dwwykc2MPK8M2HN" crossorigin="anonymous">
</head>

<body>
    <div class="container mt-3">
        <div class="card">
            <div class="card-header text-center">
                <h5>Tugas Mentoring 12</h5>
            </div>
            <div class="card-body">
                <div class="row">
                    <?php if ($this->session->flashdata('error')) : ?>
                        <div class="alert alert-danger" role="alert">
                            <?= $this->session->flashdata('error'); ?>
                        </div>
                    <?php elseif ($this->session->flashdata('success')) : ?>
                        <div class="alert alert-success" role="alert">
                            <?= $this->session->flashdata('success'); ?>
                        </div>
                    <?php endif; ?>
                </div>
                <form action="<?= base_url('index.php/upload/do_upload'); ?>" method="POST" enctype="multipart/form-data">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="mb-3">
                                <label for="dokumen" class="form-label">Upload Dokumen</label>
                                <input class="form-control" multiple="multiple" type="file" id="dokumen" name="multipleDoc[]">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="mb-3">
                                <label for="dokumen" class="form-label">Upload Gambar</label>
                                <input class="form-control" type="file" id="image" name="image">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="d-grid gap-2">
                            <button class="btn btn-success" type="submit">Simpan</button>
                        </div>
                    </div>
                </form>
                <hr>
                <h5 class="mt-3 mb-3 text-left">Daftar File Upload</h5>
                <table class="table table-bordered table-striped">
                    <tr class="text-center">
                        <th>No</th>
                        <th>File</th>
                        <th>Type</th>
                        <th>Action</th>
                    </tr>
                    <?php
                    $no = 1;
                    foreach ($data as $file) : ?>
                        <tr>
                            <td><?= $no++; ?></td>
                            <td><?= $file['path']; ?></td>
                            <td><?= $file['type']; ?></td>
                            <td class="text-center">
                                <?php if ($file['type'] == 'application/pdf') : ?>
                                    <a href="<?= base_url('public/documents/' . $file['path']); ?>" target="_blank" class="btn btn-primary">Download</a>
                                <?php else : ?>
                                    <a href="<?= base_url('public/images/' . $file['path']); ?>" target="_blank" class="btn btn-primary">Download</a>
                                <?php endif; ?>
                                <a href="<?= base_url('index.php/upload/delete?type=' . $file['type'] . '&id=' . $file['id']); ?>" class="btn btn-danger">Delete</a>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                </table>
            </div>
        </div>
    </div>
</body>

<script src=" https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-C6RzsynM9kWDrMNeT87bh95OGNyZPhcTNXj1NW7RuBCsyN/o0jlpcV8Qyq46cDfL" crossorigin="anonymous"></script>

</html>