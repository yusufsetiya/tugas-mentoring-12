<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Upload extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->helper(array('form', 'url'));
        $this->load->library('form_validation');
        $this->load->library('upload');
        $this->load->library('session');
        $this->load->database();
    }

    public function index()
    {
        $data['data'] = $this->db->get('uploaded')->result_array();
        $this->load->view('upload', $data);
    }

    public function do_upload()
    {
        if (empty($_FILES['image']['name']) && empty($_FILES['multipleDoc']['name'][0])) {
            $this->session->set_flashdata('error', 'Please select the file you want to upload first.');
            redirect('upload');
        }

        if (isset($_FILES['image']) && !empty($_FILES['image']['name'])) {
            $config['upload_path'] = './public/images/';
            $config['allowed_types'] = 'gif|jpg|png';
            $config['max_size'] = 1024;
            $config['encrypt_name'] = TRUE;

            $this->upload->initialize($config);

            if ($this->upload->do_upload('image')) {
                $upload_data = $this->upload->data();
                $file_path = '/' . $upload_data['file_name'];
                $file_type = $upload_data['file_type'];

                // insert file data into database
                $this->db->insert('uploaded', [
                    'path' => $file_path,
                    'type' => $file_type
                ]);
            } else {
                $error = $this->upload->display_errors();
                $this->session->set_flashdata('error', $error);
            }
        }

        if (!empty($_FILES['multipleDoc']['name'][0])) {
            $config['upload_path']   = './public/documents/';
            $config['allowed_types'] = 'pdf';
            $config['max_size']      = 2048;
            $config['encrypt_name']  = TRUE;

            $this->upload->initialize($config);


            $multipleFile = [];

            foreach ($_FILES['multipleDoc']['name'] as $key => $file_name) {
                $_FILES['userfile']['name']     = $_FILES['multipleDoc']['name'][$key];
                $_FILES['userfile']['type']     = $_FILES['multipleDoc']['type'][$key];
                $_FILES['userfile']['tmp_name'] = $_FILES['multipleDoc']['tmp_name'][$key];
                $_FILES['userfile']['error']    = $_FILES['multipleDoc']['error'][$key];
                $_FILES['userfile']['size']     = $_FILES['multipleDoc']['size'][$key];

                if ($this->upload->do_upload('userfile')) {

                    $multipleFile[] = $this->upload->data();
                } else {

                    $error = $this->upload->display_errors();
                    $this->session->set_flashdata('error', $error);
                }
            }

            foreach ($multipleFile as $pdf) {
                $file_path = '/' . $pdf['file_name'];
                $file_type = $pdf['file_type'];

                $this->db->insert('uploaded', [
                    'path' => $file_path,
                    'type' => $file_type
                ]);
            }
        }

        $this->session->set_flashdata('success', 'Upload file success.');

        redirect('upload');
    }

    public function delete()
    {
        $id = $this->input->get('id');

        $file = $this->db->get_where('uploaded', ['id' => $id])->row();

        if ($file->type == 'application/pdf')
            unlink('./public/documents' . $file->path);
        else {
            unlink('./public/images' . $file->path);
        }

        $this->db->delete('uploaded', ['id' => $id]);

        $this->session->set_flashdata('success', 'Delete file success.');

        redirect('upload');
    }
}
